package model;

public class Hashing {
	
	public long sumNum(String str){
		long num = 0;
		for(int i=0;i<str.length();i++){
			char chars = str.charAt(i);
			num += (long) chars;
		}
		return num;
	}
	public int modNum(String str,int numCom){
		long num = sumNum(str);
		num = num % numCom;
		return (int) num; 
	}
	
}
