package control;

import model.Hashing;
import gui.GUIHashing;

public class Chashing {
	private GUIHashing frame ;
	private Hashing hashing;
	
	public Chashing() {
		// TODO Auto-generated constructor stub
		createFrame();
		testCase();
	}

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		new Chashing();
	}
	public void createFrame(){
		frame = new GUIHashing();
		frame.setVisible(true);
		frame.setLocation(250,150);
		frame.setSize(800, 450);
		
	}
	public void testCase(){
		hashing = new Hashing();
		frame.setResult("http://www.cs.sci.ku.ac.th/~fscichj/");
		frame.setResult(hashing.modNum("http://www.cs.sci.ku.ac.th/~fscichj/", 4)+"");
		frame.setResult("https://www.youtube.com/");
		frame.setResult(hashing.modNum("https://www.youtube.com/", 4)+"");
		frame.setResult("https://docs.google.com/document/d/1D34SimpuyAX_PXjnhPv5pRlVK8xLvqPnYdTkdOSN-h8/edit");
		frame.setResult(hashing.modNum("https://docs.google.com/document/d/1D34SimpuyAX_PXjnhPv5pRlVK8xLvqPnYdTkdOSN-h8/edit", 4)+"");
		frame.setResult("https://www.google.co.th/webhp?sourceid=chrome-instant&rlz=1C1KMZB_enTH549TH549&ion=1&espv=2&es_th=1&ie=UTF-8#q=java");
		frame.setResult(hashing.modNum("https://www.google.co.th/webhp?sourceid=chrome-instant&rlz=1C1KMZB_enTH549TH549&ion=1&espv=2&es_th=1&ie=UTF-8#q=java", 4)+"");
		frame.setResult("https://www.facebook.com/groups/602554663223573/");
		frame.setResult(hashing.modNum("https://www.facebook.com/groups/602554663223573/", 4)+"");
		frame.setResult("https://docs.google.com/document/d/1FHcs1AsRsnvvWQD0HafWYRARbDBJcyUanFngqNgRKzo/edit?usp=sharing");
		frame.setResult(hashing.modNum("https://docs.google.com/document/d/1FHcs1AsRsnvvWQD0HafWYRARbDBJcyUanFngqNgRKzo/edit?usp=sharing", 4)+"");
		frame.setResult("https://www.facebook.com/groups/418250538339006/");
		frame.setResult(hashing.modNum("https://www.facebook.com/groups/418250538339006/", 4)+"");
		frame.setResult("https://www.edmodo.com/home#/group?id=12056235");
		frame.setResult(hashing.modNum("https://www.edmodo.com/home#/group?id=12056235", 4)+"");
		frame.setResult("https://bitbucket.org/");
		frame.setResult(hashing.modNum("https://bitbucket.org/", 4)+"");
		frame.setResult("http://docs.oracle.com/javase/7/docs/api/");
		frame.setResult(hashing.modNum("http://docs.oracle.com/javase/7/docs/api/", 4)+"");
		frame.setResult("http://www.wegointer.com/2014/09/harvard-healthbeat/");
		frame.setResult(hashing.modNum("http://www.wegointer.com/2014/09/harvard-healthbeat/",4)+"");
		frame.setResult("http://mashable.com/2015/01/18/programming-languages-2015/#:eyJzIjoiZiIsImkiOiJfampsNnRheXAzZ2lkZzBrYyJ9");
		frame.setResult(hashing.modNum("http://mashable.com/2015/01/18/programming-languages-2015/#:eyJzIjoiZiIsImkiOiJfampsNnRheXAzZ2lkZzBrYyJ9", 4)+"");
		
	}

}
